
<?php

/**
 * @file
 * Rules integration for Rules Mail Edit.
 */

/**
 * Restrict access to site administrators.
 */
function rules_mail_edit_access($type, $name) {
  return user_access('administer site configuration');
}

/**
 * Implements hook_rules_action_info().
 */
function rules_mail_edit_rules_action_info() {
  return array(
    'rules_mail_edit_mail' => array(
      'label' => t('Send mail using mail edit templates'),
      'group' => t('System'),
      'parameter' => array(
        'to' => array(
          'type' => 'text',
          'label' => t('To'),
          'description' => t('The e-mail address or addresses where the message will be sent to. The formatting of this string must comply with RFC 2822.'),
        ),
        'subject' => array(
          'type' => 'text',
          'label' => t('Subject'),
          'description' => t("The mail's subject."),
          'translatable' => TRUE,
        ),
        'message' => array(
          'type' => 'text',
          'label' => t('Message'),
          'description' => t("The mail's message body."),
          'translatable' => TRUE,
        ),
        'from' => array(
          'type' => 'text',
          'label' => t('From'),
          'description' => t("The mail's from address. Leave it empty to use the site-wide configured address."),
          'optional' => TRUE,
        ),
        'language' => array(
          'type' => 'text',
          'label' => t('Language'),
          'description' => t('The entity language the message should be translated to.'),
          'optional' => TRUE,
          'default value' => LANGUAGE_NONE,
        ),
      ),
      'base' => 'rules_mail_edit_action_mail',
      'access callback' => 'rules_mail_edit_access',
    ),
    'rules_mail_edit_mail_to_users_of_role' => array(
      'label' => t('Send mail to all users of a role using mail edit templates'),
      'group' => t('System'),
      'parameter' => array(
        'roles' => array(
          'type' => 'list<integer>',
          'label' => t('Roles'),
          'options list' => 'entity_metadata_user_roles',
          'description' => t('Select the roles whose users should receive the mail.'),
        ),
        'subject' => array(
          'type' => 'text',
          'label' => t('Subject'),
          'description' => t("The mail's subject."),
        ),
        'message' => array(
          'type' => 'text',
          'label' => t('Message'),
          'description' => t("The mail's message body."),
        ),
        'from' => array(
          'type' => 'text',
          'label' => t('From'),
          'description' => t("The mail's from address. Leave it empty to use the site-wide configured address."),
          'optional' => TRUE,
        ),
      ),
      'base' => 'rules_mail_edit_action_mail_to_users_of_role',
      'access callback' => 'rules_mail_edit_access',
    ),
  );
}

/**
 * Action. Send mail to all users of a specific role group(s).
 */
function rules_mail_edit_action_mail_to_users_of_role($roles, $subject, $message, $from, $settings, RulesState $state, RulesPlugin $element) {
  $from = !empty($from) ? str_replace(array("\r", "\n"), '', $from) : NULL;

  // All authenticated users, which is everybody.
  if (in_array(DRUPAL_AUTHENTICATED_RID, $roles)) {
    $result = db_query('SELECT mail FROM {users} WHERE uid > :num', array(':num' => 0));
  }
  else {
    $rids = implode(',', $roles);
    // Avoid sending emails to members of two or more target role groups.
    $result = db_query('SELECT DISTINCT u.mail, u.language FROM {users} u INNER JOIN {users_roles} r ON u.uid = r.uid WHERE r.rid IN (:rids)', array(':rids' => $rids));
  }

  // Set a unique key for this mail.
  $name      = isset($element->root()->name) ? $element->root()->name : 'unnamed';
  $key       = RULES_MAIL_EDIT_MAIL_ROLE_RULE_KEY . $name . '_' . $element->elementId();
  $languages = language_list();

  $message      = array('result' => TRUE);
  $languages    = language_list();
  $lang_default = language_default();
  $params       = array();

  _mail_edit_module_load_include('alter.inc');

  foreach ($result as $row) {
    $language = ($row->language == LANGUAGE_NONE || empty($languages[$row->language])) ? $lang_default : $languages[$row->language];

    // Store template once, when new language is found.
    if (empty($params[$row->language])) {
      $mail_edit_template = _mail_edit_load("rules_" . $key, $language, TRUR);

      if (empty($mail_edit_template)) {
        $params[$row->language] = array(
          'subject' => $subject,
          'message' => $message,
        );
      }
      else {
        $params[$row->language] = array(
          // Replace tokens.
          'subject' => _rules_mail_edit_evaluate($mail_edit_template['subject'], array(), $state),
          'message' => _rules_mail_edit_evaluate($mail_edit_template['body'], array(), $state),
          'langcode' => $row->language,
        );
      }
    }

    $message = drupal_mail('rules', $key . '_skip_mail_edit', $row->mail, $language, $params[$row->language], $from);
    if (!$message['result']) {
      break;
    }
  }
  if ($message['result']) {
    $role_names = array_intersect_key(user_roles(TRUE), array_flip($roles));
    watchdog('rules', 'Successfully sent email to the role(s) %roles.', array('%roles' => implode(', ', $role_names)));
  }
}


/**
 * Action Implementation: Send mail.
 */
function rules_mail_edit_action_mail($to, $subject, $message, $from, $langcode, $settings, RulesState $state, RulesPlugin $element) {
  $to   = str_replace(array("\r", "\n"), '', $to);
  $from = !empty($from) ? str_replace(array("\r", "\n"), '', $from) : NULL;

  // Set a unique key for this mail.
  $name      = isset($element->root()->name) ? $element->root()->name : 'unnamed';
  $key       = RULES_MAIL_EDIT_MAIL_RULE_KEY . $name . '_' . $element->elementId();
  $languages = language_list();
  $language  = ($langcode == LANGUAGE_NONE || empty($languages[$langcode])) ? language_default() : $languages[$langcode];

  // Get the mail edit template because token replacemnet is done differently.
  _mail_edit_module_load_include('alter.inc');
  $mail_edit_template = _mail_edit_load("rules_" . $key, $language, TRUE);

  $params = array(
    // Replace tokens.
    'subject' => _rules_mail_edit_evaluate($mail_edit_template['subject'], array(), $state),
    'message' => _rules_mail_edit_evaluate($mail_edit_template['body'], array(), $state),
    'langcode' => $langcode,
  );

  // Change the key to prevent loading template again.
  $message = drupal_mail('rules', $key . '_skip_mail_edit', $to, $language, $params, $from);
  if ($message['result']) {
    watchdog('rules', 'Successfully sent email to %recipient', array('%recipient' => $to));
  }
}

/**
 * Evaluate tokens.
 *
 * Pulled from RulesTokenEvaluator class. See system.eval.inc.
 */
function _rules_mail_edit_evaluate($text, $options, RulesState $state) {
  $var_info = $state->varInfo();
  $options += array('sanitize' => FALSE);

  $replacements = array();
  $data         = array();
  // We also support replacing tokens in a list of textual values.
  $whole_text   = is_array($text) ? implode('', $text) : $text;
  foreach (token_scan($whole_text) as $var_name => $tokens) {
    $var_name = str_replace('-', '_', $var_name);
    if (isset($var_info[$var_name]) && ($token_type = rules_mail_edit_system_token_map_type($var_info[$var_name]['type']))) {
      // We have to key $data with the type token uses for the variable.
      $data = rules_unwrap_data(array($token_type => $state->get($var_name)), array($token_type => $var_info[$var_name]));
      $replacements += token_generate($token_type, $tokens, $data, $options);
    }
    else {
      $replacements += token_generate($var_name, $tokens, array(), $options);
    }
  }

  // Optionally clean the list of replacement values.
  if (!empty($options['callback']) && function_exists($options['callback'])) {
    $function = $options['callback'];
    $function($replacements, $data, $options);
  }

  // Actually apply the replacements.
  $tokens = array_keys($replacements);
  $values = array_values($replacements);
  if (is_array($text)) {
    foreach ($text as $i => $text_item) {
      $text[$i] = str_replace($tokens, $values, $text_item);
    }
    return $text;
  }
  return str_replace($tokens, $values, $text);
}


/**
 * Implements hook_mail().
 *
 * Sets the message subject and body as configured.
 */
function rules_mail_edit_mail($key, &$message, $params) {
  $message['subject'] .= str_replace(array("\r", "\n"), '', $params['subject']);
  $message['body'][] = $params['message'];
}

/**
 * Process mail to all users of a role.
 */
function rules_mail_edit_mail_to_users_of_role($key, &$message, $params) {
  $message['subject'] .= str_replace(array("\r", "\n"), '', $params['subject']);
  $message['body'][] = $params['message'];
}

/**
 * Looks for a token type mapping. Defaults to passing through the type.
 */
function rules_mail_edit_system_token_map_type($type) {
  $entity_info = entity_get_info();
  if (isset($entity_info[$type]['token type'])) {
    return $entity_info[$type]['token type'];
  }
  $cache = rules_get_cache();
  if (isset($cache['data_info'][$type]['token type'])) {
    return $cache['data_info'][$type]['token type'];
  }
  return $type;
}
