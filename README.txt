-- SUMMARY --

Rules Mail Edit adds rules actions to allow rules to send emails to single
users or users of a role using Mail Edit templates. It does support rules
tokens.

-- CONFIGURATION --

* After enabling the module, rules actions will have two new options under
  System:

  - Send mail using mail edit templates
  - Send mail to all users of a role using mail edit templates

* Create an action using one of these options.

* The "to", "from", and "language" fields will be used to send the email. The
  "subject" and "body" will be overriden with mail edit templates. The language
  field is what the translation wil be based on. This module provides a
  user:locale token. So if you wanted to send an email to a node author and
  key the translation off of their locale, you would use [node:author:locale].

* Visit the mail edit templates page /admin/config/system/mail-edit. You will
  see a new mailkey listed from the Rules module.


 
